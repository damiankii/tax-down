import * as request from 'supertest'
import {App} from '../src/app'
import { initDependencies } from '../src/dependencies';
import { CreateCustomerPayload, UpdateCustomerPayload } from '../src/infra/http/models';
import { Database } from "sqlite3";
import { cfg } from '../src/utils/config';

describe("Customer App", () => {
    let app: Express.Application;
    let db: Database

    beforeAll(() => {
        const dependencies = initDependencies()
        app = new App(dependencies).getServerApp()

        db = new Database(cfg.DbPath)
        resetDB(db)
    });

    // afterAll(() => {
    //     resetDB(db)
    // })

    beforeEach(() => {
        resetDB(db)
    })

    it('it should create a new user', async () => {
        const createCustomerPayload: CreateCustomerPayload = {
            name: 'tomas',
            email: 'tomas@example.com',
            password: '12345678',
            address: 'Calle Nueva 9'
        }

        const res = await request(app).post("/customer").send(createCustomerPayload)

        expect(res.statusCode).toBe(201)
        expect(res.body.status).toBe('success')
        expect(res.body.data).toMatchObject(createCustomerPayload)
    })

    it('it should return an error when creating new user with wrong data', async () => {
        const createCustomerPayload = {
            email: 'tomas@example.com',
            password: '12345678',
            address: 'Calle Nueva 9'
        }

        const res = await request(app).post("/customer").send(createCustomerPayload)

        expect(res.statusCode).toBe(422)
        expect(res.body.status).toBe('invalid_data')
        expect(res.body.data.length).toBeGreaterThanOrEqual(1)
    })

    it('it should return a user by id', async () => {
        const createCustomerPayload: CreateCustomerPayload = {
            name: 'tomas',
            email: 'tomas@example.com',
            password: '12345678',
            address: 'Calle Nueva 9'
        }

        const resCreate = await request(app).post("/customer").send(createCustomerPayload)
        const res = await request(app).get(`/customer/${resCreate.body.data.id}`)

        expect(res.statusCode).toBe(200)
        expect(res.body.status).toBe('success')
        expect(res.body.data).toMatchObject(resCreate.body.data)
    })

    it('it should return a error if user is not found', async () => {
        const res = await request(app).get(`/customer/15252`)
        expect(res.statusCode).toBe(404)
    })


    it('it should update a user', async () => {
        const createCustomerPayload: CreateCustomerPayload = {
            name: 'tomas',
            email: 'tomas@example.com',
            password: '12345678',
            address: 'Calle Nueva 9'
        }

        const createdRes = await request(app).post("/customer").send(createCustomerPayload)
        const createdUser = createdRes.body.data;

        const updateCustomerPayload: UpdateCustomerPayload = {
            name: 'jose',
            address: "calle serrano 1"
        }

        const res = await request(app).put(`/customer/${createdUser.id}`).send(updateCustomerPayload)
        expect(res.statusCode).toBe(200)
        expect(res.body.status).toBe('success')
        expect(res.body.data).toMatchObject(updateCustomerPayload)
    })

    it('it should return an error when updating a user with wrong params', async () => {
        const createCustomerPayload: CreateCustomerPayload = {
            name: 'tomas',
            email: 'tomas@example.com',
            password: '12345678',
            address: 'Calle Nueva 9'
        }

        const createdRes = await request(app).post("/customer").send(createCustomerPayload)
        const createdUser = createdRes.body.data;

        const updateCustomerPayload = {
            address: "calle serrano 1"
        }

        const res = await request(app).put(`/customer/${createdUser.id}`).send(updateCustomerPayload)
        expect(res.statusCode).toBe(422)
        expect(res.body.status).toBe('invalid_data')
        expect(res.body.data.length).toBeGreaterThanOrEqual(1)
    })

    it('it should return an empty array when there arent any users', async () => {
        const res = await request(app).get(`/customer`)
        expect(res.statusCode).toBe(200)
        expect(res.body.data.length).toBe(0)
    })

    it('it should return an array of users', async () => {
        for (let i = 0; i < 5; i++) {
            const createCustomerPayload: CreateCustomerPayload = {
                name: 'tomas',
                email: 'tomas@example.com',
                password: '12345678',
                address: 'Calle Nueva 9'
            }
    
            await request(app).post("/customer").send(createCustomerPayload)
        }
        const res = await request(app).get(`/customer`)

        expect(res.statusCode).toBe(200)
        expect(res.body.status).toBe('success')
        expect(res.body.data.length).toBe(5)
    })


    it('it should delete an user', async () => {
        const createCustomerPayload: CreateCustomerPayload = {
            name: 'tomas',
            email: 'tomas@example.com',
            password: '12345678',
            address: 'Calle Nueva 9'
        }

        const createdRes = await request(app).post("/customer").send(createCustomerPayload)
        const createdUser = createdRes.body.data;

        const deleteRes = await request(app).delete(`/customer/${createdUser.id}`);
        expect(deleteRes.status).toBe(200);

        const fetchUserRes = await request(app).get(`/customer/${createdUser.id}`); 
        expect(fetchUserRes.status).toBe(404)

    })


    it('it should add credit correctly to an user', async () => {
        const createCustomerPayload: CreateCustomerPayload = {
            name: 'tomas',
            email: 'tomas@example.com',
            password: '12345678',
            address: 'Calle Nueva 9'
        }

        const createdRes = await request(app).post("/customer").send(createCustomerPayload)
        const createdUser = createdRes.body.data;

        const setAvailableCreditPaylaod = {
            amount: 200
        }

        const addCreditRes = await request(app).put(`/customer/${createdUser.id}/available-credit`).send(setAvailableCreditPaylaod)

        expect(addCreditRes.status).toBe(200)
        expect(addCreditRes.body.data.availableCredit).toBe(200)
    })
})

function resetDB(db: Database) {
    db.run(`DELETE from customers`);
}