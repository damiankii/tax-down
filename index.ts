require('dotenv').config();
import { App } from "./src/app";
import { initDependencies } from "./src/dependencies";

export async function main() {
    const dependencies = initDependencies();
    const app = new App(dependencies);

    return app.start();
}

main()