import { Customer, CustomerRepository } from "../domain/customer";

export class CreditService {
    constructor(private _repo: CustomerRepository) {}

    async addAvailableCredit(customerId: number, amount: number): Promise<Customer> {
        const customer = await this._repo.findById(customerId);
        customer.addAvailableCredit(amount);
        return this._repo.update(customerId, customer);
    }
}