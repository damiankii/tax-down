import { Customer, CustomerRepository, CustomerSortOptions } from "../domain/customer";
import { CreateCustomerPayload, UpdateCustomerPayload } from "../infra/http/models";

export interface GetCustomerOptions {
    sort: CustomerSortOptions
}

export class CustomerService {
    constructor(protected _repo: CustomerRepository) {}

    async createCustomer(payload: CreateCustomerPayload): Promise<Customer> {
        const {name, email, password, address} = payload as CreateCustomerPayload;
        const customer = new Customer(name, email, password, address);
        return this._repo.create(customer);
    }

    async getCustomerById(id: number): Promise<Customer> {
        return this._repo.findById(id);
    }

    async getCustomers(options?: GetCustomerOptions): Promise<Customer[]> {
        let customers = await this._repo.findAll();

        if (this._isValidSortOption(options)) {
            customers = customers.sort((a, b) => {
                return (a[options!.sort] > b[options!.sort]) ? 1 : -1;
            });
        }
        return customers;
    }

    private _isValidSortOption(options?: GetCustomerOptions): Boolean {
        return !!options && options.sort && options.sort in CustomerSortOptions;
    }

    async updateCustomer(id: number, data: UpdateCustomerPayload): Promise<Customer> {
        const customer = await this._repo.findById(id);
        customer.name = data.name;
        customer.address = data.address;

        return this._repo.update(id, customer);
    }

    async deleteCustomer(id: number): Promise<void> {
        return this._repo.delete(id);
    }
}