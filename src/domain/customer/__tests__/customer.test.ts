import { Customer } from "../customer"

describe("Customer", () => {
    it("it sets Id correctly", () => {
        const customer = new Customer("John doe", "john@doe.com", "12345", "C/ Nueva 7")

        customer.setId(1)
        expect(customer.id).toBe(1)
    })

    it('it set available credit correctly', () => {
        const customer = new Customer("John doe", "john@doe.com", "12345", "C/ Nueva 7")

        customer.setAvailableCredit(100)
        expect(customer.availableCredit).toBe(100)
    })

    it("it adds customer available credit correctly", () => {
        const customer = new Customer("John doe", "john@doe.com", "12345", "C/ Nueva 7")

        customer.setAvailableCredit(100)
        customer.addAvailableCredit(400)
        expect(customer.availableCredit).toBe(500)
    })
})