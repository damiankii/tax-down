export enum CustomerSortOptions {
    ID = "id",
    NAME = "name",
    AVAILABLE_CREDIT = "availableCredit",
}

export class Customer {
    public id!: number;
    public availableCredit: number;

    constructor(
        public name: string,
        public email: string,
        public password: string,
        public address: string,
    ) {
        this.availableCredit = 0;
    }

    setId(id: number) {
        this.id = id;
        return this;
    }

    setAvailableCredit(credit: number) {
        this.availableCredit = credit;
        return this;
    }

    addAvailableCredit(credit: number) {
        this.availableCredit += credit;
        return this;
    }
}

