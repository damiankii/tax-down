import { Customer } from "./customer";

export interface CustomerRepository {
    create(customer: Customer): Promise<Customer>
    findAll(): Promise<Customer[]>
    findById(id: number): Promise<Customer>
    update(id: number, customer: Customer): Promise<Customer>
    delete(id: number): void | Promise<void>
}