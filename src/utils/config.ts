import * as config from 'config';

export interface Config {
    Http: {
        Port: number
    }
    DbPath: string
}

export const cfg: Config = {
    Http: {
        Port: config.get('app.http.port'),
    },
    DbPath: config.get('app.dbPath')
}