import { Dependencies } from "./dependencies";
import { HTTPServer } from "./infra/http";

export class App {
    private _httpServer: HTTPServer;
    constructor(protected _dependencies: Dependencies) {
        this._httpServer = new HTTPServer(this._dependencies);
    }

    public getServerApp(): Express.Application {
        return this._httpServer.getApp();
    } 

    async start(): Promise<void> {
        await this._httpServer.start();
    }

    async stop(): Promise<void> {
        this._httpServer.stop()
    }
}