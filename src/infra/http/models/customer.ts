export interface CreateCustomerPayload {
    name: string;
    email: string;
    password: string;
    address: string;
}

export interface UpdateCustomerPayload {
    name: string;
    address: string;
}