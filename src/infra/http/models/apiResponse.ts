export interface ApiResponse {
    status: ApiResponseStatus,
    data?: any
}

export enum ApiResponseStatus {
    Success = 'success',
    InvalidData = 'invalid_data'
}

export class ApiResponseFactory {
    constructor(protected status: ApiResponseStatus) { }

    build(data?: any): ApiResponse {
        return {
            status: this.status,
            data: data ? data : undefined
        };
    }
}