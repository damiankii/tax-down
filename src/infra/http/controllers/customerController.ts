import { NextFunction, Request, Response } from "express";
import { CustomerSortOptions } from "../../../domain/customer";
import { CustomerService } from "../../../services/customerService";
import { CreateCustomerPayload, UpdateCustomerPayload, ApiResponseFactory, ApiResponseStatus } from "../models";

export class CustomerController {
    constructor(protected _customerService: CustomerService) {}

    public async createCustomer(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        const result = await this._customerService.createCustomer(req.body as CreateCustomerPayload);
        return res.status(201).json(new ApiResponseFactory(ApiResponseStatus.Success).build(result));
    }

    public async getCustomer(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            const customerId = Number(req.params["id"]);
            const customer = await this._customerService.getCustomerById(customerId);

            return res.status(200).json(new ApiResponseFactory(ApiResponseStatus.Success).build(customer));
        } catch (e: any) {
            console.log(`❌ createCustomer error: ${(e as Error).message}`)
            if ((e as Error).message === 'USER_NOT_FOUND') {
                return res.sendStatus(404)
            }

            return next(e)
        }

    }

    public async getCustomers(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            const options = req.query["sort"] ? { sort: req.query["sort"] as CustomerSortOptions } : undefined;
            const customers = await this._customerService.getCustomers(options);
            return res.status(200).json(new ApiResponseFactory(ApiResponseStatus.Success).build(customers));
        } catch (e: any) {
            console.log(`❌ getCustomers error: ${(e as Error).message}`)
            return next(e)
        }
    }


    public async updateCustomer(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            const customerId = Number(req.params["id"]);
            const customer = await this._customerService.updateCustomer(customerId, req.body as UpdateCustomerPayload);

            return res.status(200).json(new ApiResponseFactory(ApiResponseStatus.Success).build(customer));
        } catch (e: any) {
            console.log(`❌ updateCustomer error: ${(e as Error).message}`)
            return next(e)
        }
    }

    public async deleteCustomer(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            const customerId = Number(req.params["id"]);
            await this._customerService.deleteCustomer(customerId);
            return res.sendStatus(200);
        } catch (e: any) {
            console.log(`❌ deleteCustomer error: ${(e as Error).message}`)
            return next(e)
        }
    }
}