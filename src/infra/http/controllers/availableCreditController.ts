import { Request, Response, NextFunction } from "express";
import { CreditService } from "../../../services/creditService";
import { ApiResponseFactory, ApiResponseStatus } from "../models";
import { AddAvailableCreditPayload } from "../models/availableCredit";

export class AvailableCreditController {

    constructor(protected _creditService: CreditService) { }

    public async addAvailableCredit(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            const customerId = Number(req.params["id"]);
            const { amount } = req.body as AddAvailableCreditPayload;

            const customer = await this._creditService.addAvailableCredit(customerId, amount);

            return res.status(200).json(new ApiResponseFactory(ApiResponseStatus.Success).build(customer));
        } catch (e: any) {
            console.log(`❌ addAvailableCredit error: ${(e as Error).message}`);
            return next(e);
        }
    }
}