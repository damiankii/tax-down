import { Router } from "express";
import { AvailableCreditController, CustomerController } from "../controllers";
import { validationMiddleware } from "../middleware";
import { createCustomerSchema, updateCustomerSchema, addAvailableCreditSchema } from "../validators";

export function customerRouter(
    customerCtrl: CustomerController,
    availableCreditCtrl: AvailableCreditController
): Router {
    const router = Router();

    router.get("/", customerCtrl.getCustomers.bind(customerCtrl));
    router.post("/", validationMiddleware(createCustomerSchema), customerCtrl.createCustomer.bind(customerCtrl));
    router.get("/:id", customerCtrl.getCustomer.bind(customerCtrl));
    router.put("/:id", validationMiddleware(updateCustomerSchema), customerCtrl.updateCustomer.bind(customerCtrl));
    router.delete("/:id", customerCtrl.deleteCustomer.bind(customerCtrl));

    router.put("/:id/available-credit", validationMiddleware(addAvailableCreditSchema), availableCreditCtrl.addAvailableCredit.bind(availableCreditCtrl));

    return router;
}