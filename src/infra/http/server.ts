import { Dependencies } from "../../dependencies";
import * as express from 'express'
import * as http from 'http';
import { customerRouter } from "./routes/customer";

export class HTTPServer {
    private _express: express.Express;
    private _server!: http.Server;
    constructor(protected _dependencies: Dependencies) {
        this._express = express()
        this._express.use(express.json())
        this._configureRoutes()
    }

    protected _configureRoutes() {
        this._express.use(
            "/customer",
            customerRouter(
                this._dependencies.controllers.customer,
                this._dependencies.controllers.availableCredit
            ))
    }

    // For testing only
    public getApp(): express.Application {
        return this._express
    }

    public async start(port = this._dependencies.config.Http.Port): Promise<void> {
        this._server = await this._express.listen(port, () => {
            console.log(`🚀 Server started at http://127.0.0.1:${port}`);
        })
    }

    public async stop() {
        await this._server.close();
    }
}