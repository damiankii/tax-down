import * as Joi from "joi";
import { Handler, NextFunction, Request, Response } from "express";
import { ApiResponseFactory, ApiResponseStatus } from "../models/apiResponse";

export function validationMiddleware<T>(schema: Joi.Schema): Handler {
    return (req: Request, res: Response, next: NextFunction) => {
        const { error } = schema.validate(req.body as T);
        if (!error) {
            return next();
        }

        return res.status(422).send(new ApiResponseFactory(ApiResponseStatus.InvalidData).build(error.details));
    }
}