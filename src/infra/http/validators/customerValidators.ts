import * as Joi from "joi";

export const createCustomerSchema = Joi.object({
    name: Joi.string().min(3).required(),
    email: Joi.string().email().required(),
    password: Joi.string().alphanum().min(3).required(),
    address: Joi.string().required()
})

// For the sake of the exercise we don't allow the user to change password and email using this endpoint
export const updateCustomerSchema = Joi.object({
    name: Joi.string().min(3).required(),
    address: Joi.string().required()
})