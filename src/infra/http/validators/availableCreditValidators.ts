import * as Joi from "joi";

export const addAvailableCreditSchema = Joi.object({
    amount: Joi.number().positive()
});