CREATE TABLE IF NOT EXISTS customers (
	id INTEGER primary key autoincrement,
	name TEXT not null,
	email TEXT not null,
	password text not null,
	address text not null,
	available_credit INTEGER DEFAULT 0
);
