import { Customer, CustomerRepository } from "../../../domain/customer";
import { Database } from "sqlite3";
import { DBModelMapper } from "./dbModelMapper";

export interface IDBCustomer {
    id: number;
    name: string;
    email: string;
    password: string;
    address: string;
    available_credit: number;
}


export class SqliteCustomerRepository implements CustomerRepository {
    private db: Database;

    constructor(dbFilePath: string) {
        this.db = new Database(dbFilePath);
    }

    async create(customer: Customer): Promise<Customer> {
        const query = `INSERT INTO customers (name, email, password, address, available_credit) VALUES (?,?,?,?,?)`
        const props = [customer.name, customer.email, customer.password, customer.address, customer.availableCredit]

        return new Promise((resolve, reject) => {
            this.db.run(query, props, function (err: any, res: any) {
                if (err) {
                    return reject(err)
                }
                // @ts-ignore
                customer.setId(this.lastID)

                return resolve(customer)
            })
        })

    }
    findAll(): Promise<Customer[]> {
        return new Promise((resolve, reject) => {
            this.db.all(
                "SELECT * FROM CUSTOMERS",
                (err: any, rows: IDBCustomer[]) => {
                    if (err) {
                        return reject(err)
                    }

                    return resolve(rows.map(c => DBModelMapper.toDomain(c)))
                }
            )
        })
    }
    findById(id: number): Promise<Customer> {
        const query = "SELECT * FROM customers WHERE id = ?"
        const props = [id]
        return new Promise((resolve, reject) => {
            this.db.get(
                query,
                props,
                (err: any, row: IDBCustomer) => {
                    if (err) {
                        return reject(err)
                    }
                    if (!row) {
                        return reject(new Error("USER_NOT_FOUND"))
                    }
                    return resolve(DBModelMapper.toDomain(row))
                }
            )
        })
    }
    update(id: number, customer: Customer): Promise<Customer> {
        const query = "UPDATE customers SET name = ?, email = ?, password = ? , address = ?, available_credit = ? WHERE id = ?"
        const props = [customer.name, customer.email, customer.password, customer.address, customer.availableCredit, id]

        return new Promise((resolve, reject) => {
            this.db.run(
                query,
                props,
                (err: any) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(customer)
                }
            )
        })
    }
    delete(id: number): void {
        this.db.run("DELETE FROM customers WHERE id = ?", [id])
        return;
    }

}