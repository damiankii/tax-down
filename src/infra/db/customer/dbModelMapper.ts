import { Customer } from "../../../domain/customer";
import { IDBCustomer } from "./sqliteCustomerRepo";

export class DBModelMapper {
    static toDomain(dbCustomer: IDBCustomer): Customer {
        const customer = new Customer(dbCustomer.name, dbCustomer.email, dbCustomer.password, dbCustomer.address)
        customer.setAvailableCredit(dbCustomer.available_credit)
        customer.setId(dbCustomer.id)

        return customer
    }
}