import { CustomerRepository } from "./domain/customer"
import { SqliteCustomerRepository } from "./infra/db/customer/sqliteCustomerRepo"
import { AvailableCreditController } from "./infra/http/controllers/availableCreditController"
import { CustomerController } from "./infra/http/controllers/customerController"
import { CreditService } from "./services/creditService"
import { CustomerService } from "./services/customerService"
import { cfg, Config } from "./utils/config"


export interface Services {
    customer: CustomerService
    credit: CreditService
}

export interface Controllers {
    customer: CustomerController,
    availableCredit: AvailableCreditController
}
export interface Repositories {
    customer: CustomerRepository
}


export interface Dependencies {
    config: Config
    services: Services
    controllers: Controllers
    repositories: Repositories
}

export class ImplDependencies implements Dependencies {
    protected _services: Services | undefined;
    protected _controllers: Controllers | undefined;
    protected _repositories: Repositories | undefined;

    constructor(public config: Config) { }

    setServices(services: Services): this {
        this._services = services;
        return this;
    }

    setControllers(controllers: Controllers): this {
        this._controllers = controllers;
        return this;
    }

    setRepositories(repositores: Repositories): this {
        this._repositories = repositores;
        return this;
    }

    get services(): Services {
        if (!this._services) {
            throw new TypeError('Services should be defined before use');
        }

        return this._services;
    }

    get controllers(): Controllers {
        if (!this._controllers) {
            throw new TypeError('Controllers should be defined before use');
        }

        return this._controllers;
    }

    get repositories(): Repositories {
        if (!this._repositories) {
            throw new TypeError('Repositories should be defined before use');
        }

        return this._repositories;
    }
}

export function initDependencies(): Dependencies {
    const deps = new ImplDependencies(cfg);

    // Repos
    const customerRepo = new SqliteCustomerRepository(deps.config.DbPath);

    // Services
    const customerService = new CustomerService(customerRepo);
    const creditService = new CreditService(customerRepo);

    deps.setRepositories({
        customer: customerRepo
    });
    deps.setServices({
        customer: customerService,
        credit: creditService
    });
    deps.setControllers({
        customer: new CustomerController(customerService),
        availableCredit: new AvailableCreditController(creditService)
    });
    
    return deps;
}