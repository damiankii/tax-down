require('dotenv').config();
import { promises as fs } from 'fs';
import { Database } from "sqlite3";
import { cfg } from '../src/utils/config'

async function setUpDB() {
    const db = new Database(cfg.DbPath)
    const migrationDir = "./src/infra/db/migrations"
    const files = await fs.readdir(migrationDir);

    for (const file of files) {
        const filePath = `${migrationDir}/${file}`;
        const query = await fs.readFile(filePath, 'utf-8');
        await db.exec(query);
    }
}

setUpDB();