# README

## Requirments
- Node 18+

## Usage
To install dependencies run: 
- `npm install`

To setup the database run:
- `npm run setupDB`

To start the app run:
- `npm run start`

To run the tests run:
- `npm run test`